public class MethodsTest
{
	public static void main(String [] args)
	{
		SecondClass sc = new SecondClass() ;
		int x = sc.addOne(50);
		System.out.println(x);
		int y = sc.addTwo(50);
		System.out.println(y);
	}
	public static void methodnoInputNoReturn()
	{
		System.out.println("inside the method no input no return");
		int x = 10;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int y)
	{
		System.out.println("inside the method one input no return");
		System.out.println(y);
	}
	public static void methodTwoInputNoReturn(int n1, double n2)
	{
		System.out.println("inside the method two input no return");
		System.out.println(n1 + " " + n2);
	}
	
	public static int methodNoInputReturnInt()
	{
		System.out.println("inside the method no input one return int");
		return 6;
	}
	public static double sumSquareRoot(int n1, int n2)
	{
		int add = n1 + n2;
		double sqr = Math.sqrt(add);
		return sqr;
	}
}

