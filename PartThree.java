import java.util.Scanner;
public class PartThree
{
	public static void main(String args[])
	{
		AreaComputations ac = new AreaComputations();
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the measurement of one side of the square");
		double squareSide = scan.nextDouble();
		
		System.out.println("Enter the measurement of the length of the rectangle");
		double recLen = scan.nextDouble();
		
		System.out.println("Enter the measurement of the width of the rectangle");
		double recWid = scan.nextDouble();
		
		System.out.println("The area of the square is: " + ac.areaSquare(squareSide));
		System.out.println("The area of the rectangle is: " + ac.areaRectangle(recLen, recWid));
	}
	
}